<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create vehicle</title>
    <link href="/css/main.css" rel="stylesheet">
</head>
<body>
<h2 class="hello-title">Create vehicle!</h2>
<form:form method="POST"
           action="/createVehicle" modelAttribute="vehicleForm">
    <table>
        <tr>
            <td><form:label path="model">Model</form:label></td>
            <td><form:select path="model" items="${allModels}"/></td>
        </tr>
        <tr>
            <form:errors path="model" cssClass="error" element="td"/>
        </tr>
        <tr>
            <td><form:label path="horsepower">Horsepower</form:label></td>
            <td><form:input path="horsepower"/></td>
        </tr>
        <tr>
            <form:errors path="horsepower" cssClass="error" element="td"/>
        </tr>
        <tr>
            <td><input type="submit" value="Submit"/></td>
        </tr>
    </table>
</form:form>
<script src="/js/main.js"></script>
</body>
</html>