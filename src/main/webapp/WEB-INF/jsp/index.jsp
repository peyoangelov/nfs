<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hello ${name}!</title>
    <link href="/css/main.css" rel="stylesheet">
</head>
<body class="container">
<h2 class="hello-title">Hello ${name}!</h2>
<a href="/createVehicle">Click for create </a>
${success}

<div class="row">
    <div class="col-sm-6">
       <b>Model</b>
    </div>
    <div class="col-sm-6">
        <b>Horse</b>
    </div>
</div>
<c:forEach items="${allVehicles}" var="car">
    <div class="row">
        <div class="col-sm-6">
                ${car.model}
        </div>
        <div class="col-sm-6">
                ${car.horsepower}
        </div>
    </div>

</c:forEach>


<script src="/js/main.js"></script>
</body>
</html>