package com.angelov.nfs;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
public class NfsApplication {

    public static void main(String[] args) {
        SpringApplication.run(NfsApplication.class, args);
    }
}
