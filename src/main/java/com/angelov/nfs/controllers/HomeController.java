package com.angelov.nfs.controllers;

import com.angelov.nfs.services.vehicle.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpSession;

@Controller
public class HomeController {

    @Autowired
    private VehicleService vehicleService;

    @GetMapping("/")
    public String getIndex(HttpSession session, Model model){
        model.addAttribute("allVehicles",vehicleService.getAllVehicles());
        return "index";
    }

}
