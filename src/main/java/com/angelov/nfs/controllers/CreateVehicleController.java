package com.angelov.nfs.controllers;

import com.angelov.nfs.enums.VehicleModel;
import com.angelov.nfs.forms.VehicleForm;
import com.angelov.nfs.services.vehicle.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Arrays;

@Controller
public class CreateVehicleController {
    @Autowired
    private VehicleService vehicleService;

    @GetMapping("/createVehicle")
    public String getCreateVehiclePage(Model model) {
        model.addAttribute("vehicleForm", new VehicleForm());
        model.addAttribute("allModels", Arrays.asList(VehicleModel.values()));
        return "createVehicle";
    }

    @PostMapping("/createVehicle")
    public String createVehicle(Model model, @Valid VehicleForm vehicleForm, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if(bindingResult.hasErrors()){
            model.addAttribute("vehicleForm", vehicleForm);
            model.addAttribute("allModels", Arrays.asList(VehicleModel.values()));
            return "createVehicle";
        }
        vehicleService.saveVehicle(vehicleForm);
        redirectAttributes.addFlashAttribute("success", "created!");

        return "redirect:/";
    }

}
