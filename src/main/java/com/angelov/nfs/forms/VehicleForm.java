package com.angelov.nfs.forms;


import org.springframework.format.annotation.NumberFormat;

import javax.persistence.Column;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class VehicleForm {
    @NotNull
    private String model;
    @NotNull(message = "Can not be empty!")
    @Min(value = 2,message = "HP must be greater than 1")
    @NumberFormat(style = NumberFormat.Style.NUMBER)
    private Integer horsepower;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getHorsepower() {
        return horsepower;
    }

    public void setHorsepower(Integer horsepower) {
        this.horsepower = horsepower;
    }
}
