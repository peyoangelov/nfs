package com.angelov.nfs.services.dtos;

import com.angelov.nfs.enums.VehicleModel;

public class CreateVehicleDto {
    private VehicleModel model;
    private Integer horsepower;

    public CreateVehicleDto() {
    }

    public CreateVehicleDto(VehicleModel model, Integer horsepower) {
        this.model = model;
        this.horsepower = horsepower;
    }

    public VehicleModel getModel() {
        return model;
    }

    public void setModel(VehicleModel model) {
        this.model = model;
    }

    public Integer getHorsepower() {
        return horsepower;
    }

    public void setHorsepower(Integer horsepower) {
        this.horsepower = horsepower;
    }
}
