package com.angelov.nfs.services.vehicle;

import com.angelov.nfs.domains.Vehicle;
import com.angelov.nfs.enums.VehicleModel;
import com.angelov.nfs.forms.VehicleForm;
import com.angelov.nfs.repositories.VehicleRepository;
import com.angelov.nfs.services.dtos.CreateVehicleDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class VehicleServiceImpl implements VehicleService {
    @Autowired
    private VehicleRepository vehicleRepository;


    @Override
    public void saveVehicle(VehicleForm form) {
        final Vehicle vehicle = createVehicle(form);
        vehicleRepository.save(vehicle);
    }

    @Override
    public List<Vehicle> getAllVehicles() {
        List<Vehicle> result = new ArrayList<>();
        vehicleRepository.findAll().forEach(result::add);
        return result;
    }


    private Vehicle createVehicle(final VehicleForm form) {
        final Vehicle vehicle = new Vehicle();
        vehicle.setHorsepower(form.getHorsepower());
        vehicle.setModel(Enum.valueOf(VehicleModel.class,form.getModel()));
        return vehicle;
    }
}
