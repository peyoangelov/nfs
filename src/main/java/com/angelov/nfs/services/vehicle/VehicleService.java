package com.angelov.nfs.services.vehicle;

import com.angelov.nfs.domains.Vehicle;
import com.angelov.nfs.forms.VehicleForm;

import java.util.List;

public interface VehicleService {
    void saveVehicle(final VehicleForm form);
    List<Vehicle> getAllVehicles();
}
