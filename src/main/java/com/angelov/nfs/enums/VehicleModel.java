package com.angelov.nfs.enums;

public enum VehicleModel {
    FORD, BWM, VOLKSWAGEN
}
